package Ku.sci.cs.appointment;

import java.util.Date;

//Amornthep Rojanasarit 5610450373
public class Task implements appoint_task  {
	Date date;
	String description;
	boolean status=false;
	String blank="y";
	
	public Task(Date d,String des){
		date = d;
		description = des;
	}
	public Task(String des){
		blank = "n";
		description = des;
	}
	
	public void set_success(){
		status = true;
	}
	public boolean get_status(){
		return status;
	}
	
	public String toString(){
		if(!blank.equals("y")){
			return "Task[Description="+description+", Status = "+status+"]";
		}
		else{
		String d =""+date;
		return "Task[date="+d.substring(0,10)+d.substring(23,28)+",Description="+description+"Status = "+status+"]";
		}
	}
	
}
