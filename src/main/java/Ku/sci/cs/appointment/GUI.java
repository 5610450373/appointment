package Ku.sci.cs.appointment;
//Amornthep Rojanasarit 5610450373
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class GUI extends JFrame {
	Appointment_Manager app_manager;
	JTextArea showResult;
	JPanel pan;
	JPanel pan1;
	JPanel pan2;
	JPanel pan3;
	JTextField gettext1;
	JTextField gettext2;
	JTextField gettext3;
	JTextField gettext4;
	JLabel message;
	JComboBox<String> typebox;
	JButton add1;
	JButton add2;
	JButton cleardata;
	public GUI(){
		app_manager = new Appointment_Manager();
		//ApplicationContext bf =  new ClassPathXmlApplicationContext("Appointment_Manager.xml");
	 	//app_manager = (Appointment_Manager) bf.getBean("app_manager");
		
		
		createFrame();
		pack();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		setSize(1300,400);
	}
	
	public void createFrame(){
		app_manager.readdata();
		/*data = file.read();
		for(String[] s : data){
			if(s[0].equals("app")){
				DateFormat dateTimeFormat = new SimpleDateFormat("dd/MM/yy HH:mm");
				try {
					Date myTime = dateTimeFormat.parse(s[1]);
					//book.add(new Onetime(myTime,s[2]));
					book.add(fac.getApp(s[3],myTime,s[2]));
					
				} catch (ParseException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
					
			}
			else{
				DateFormat dateTimeFormat2 = new SimpleDateFormat("dd/MM/yy");
				if(s[1].equals("n")||(s[1].equals("N"))){
					book.add(new Task(s[2]));	
				}
				else{
					try {
						Date myTime = dateTimeFormat2.parse(s[1]);
						book.add(new Task(myTime,s[2]));
					} catch (ParseException e1) {
						e1.printStackTrace();
					}
				}
		}
		}*/
		
		pan = new JPanel();
		pan1 = new JPanel();
		pan3 = new JPanel();
		//showallResult = new JButton("show all");
		showResult = new JTextArea();
		showResult.setText(app_manager.getbookstring());
		message = new JLabel("MESSAGE :");
		pan.setLayout(new GridLayout(7,1));
		pan3.setLayout(new BorderLayout());
		pan3.add(showResult);
		pan3.add(message,BorderLayout.NORTH);
		
		
		pan.add(new JLabel("Appointment"));
		
		pan1.setLayout(new GridLayout(1, 6));
		gettext1 = new JTextField();
		gettext2 = new JTextField();
		gettext3 = new JTextField();
		gettext4 = new JTextField();
		typebox = new JComboBox<>();
		typebox.addItem("Onetime");
		typebox.addItem("Daily");
		typebox.addItem("Weekly");
		typebox.addItem("Monthly");
		pan1.add(new JLabel("DD/MM/yy HH:mm :"));
		pan1.add(gettext1);
		pan1.add(new JLabel("Description :"));
		pan1.add(gettext2);
		pan1.add(new JLabel("Select type"));
		pan1.add(typebox);
		pan.add(pan1);
		
		add1 = new JButton("ADD Appointment");
		add1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
			String type = (String) typebox.getSelectedItem();
			String des = gettext2.getText();	
			gettext1.setText("");
			gettext2.setText("");
			message.setText(app_manager.add_appoint(type, gettext1.getText(), des));
			showResult.setText(app_manager.getbookstring());	
			}
		});
		pan.add(add1);
		
		pan.add(new JLabel("Task"));
		pan2 = new JPanel();
		pan2.setLayout(new GridLayout(1, 4));
		pan2.add(new JLabel("(DD/MM/yy) or enter: n"));
		pan2.add(gettext3);
		pan2.add(new JLabel("Description :"));
		pan2.add(gettext4);
		pan.add(pan2);
		
		add2 = new JButton("ADD Task");
		add2.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
			String date = gettext3.getText();
			String des = gettext4.getText();
			
			message.setText(app_manager.add_task((String) typebox.getSelectedItem(), date, des));
			showResult.setText(app_manager.getbookstring());
			}
		});
		pan.add(add2);
		cleardata = new JButton("CLEAR TEXT");
		cleardata.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				//file.reset();
				app_manager.clearbook();
				showResult.setText(app_manager.getbookstring());
			}
		});
		
		pan.add(cleardata);
		setLayout(new GridLayout(1, 2));
		add(pan3);
		add(pan);
		
	}
}
