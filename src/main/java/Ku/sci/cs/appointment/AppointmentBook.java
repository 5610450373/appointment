package Ku.sci.cs.appointment;
import java.util.List;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

// Amornthep Rojanasarit 5610450373

public class AppointmentBook {
	ArrayList<String> lis;
	ArrayList<appoint_task> lis2;
	
	public AppointmentBook(){
		lis = new ArrayList<String>();
		lis2 = new ArrayList<appoint_task>();
	}
	public void addAll(List<appoint_task> list){
		for(int i=0;i==list.size();i++){
			lis.add(list.get(i).toString());
			lis2.add(list.get(i));
		}
	}
	 public void add(appoint_task apt){
		 lis.add(apt.toString());
		 lis2.add(apt);
	 }
	 public void add(String date,String desc){
		 String ss = "Description :"+desc+"\n"+"Date :"+date;
		 lis.add(ss);
		 
	 }
	 public int getNumAppointment(){
		 return lis.size();
	 }
	 
	 public String getAppoint(int index){
		 return lis.get(index);
	 }
	 
	 public String toString(){
		 if(lis.size()!=0){
		 String ss = "<<< My current appoint Book >>>";
		 String ss2="<<< My current Task Book >>>";
		 for(appoint_task a : lis2){
			 if(a instanceof Appointment){
				 ss += "\n"+a.toString();
			 }
			 else{
				 ss2 += "\n"+a.toString();
			 }
		 }
		 return ss+"\n"+ss2;
		 }
		 return "";
	 }
	 public boolean occurOn(String date) throws ParseException{
		DateFormat dateTimeFormat = new SimpleDateFormat("dd/MM/yy");
		Date myTime = dateTimeFormat.parse(date);
		String t = ""+myTime;
		for(appoint_task a : lis2){
			if(a instanceof Appointment){
			if(a instanceof Daily){
				return true;
			}
		}
		}
		for(int i=0;i<lis.size();i++){
			if(lis2 .get(i) instanceof Appointment){
			if(lis.get(i).substring(17,27).equals(t.substring(0,10))&&(lis.get(i).substring(41,45).equals(t.substring(24,28)))){
				return true;
			}
			if(lis.get(i).substring(17,20).equals(t.substring(0,3))&&(lis2.get(i) instanceof Weekly)){
				return true;
			}
			if(lis.get(i).substring(25,27).equals(t.substring(8,10))&&(lis2.get(i) instanceof Monthly)){
				return true;
			}
		}
		}
		return false;
	 }
	 
	 public void clear(){
		 lis = new ArrayList<String>();
		lis2 = new ArrayList<appoint_task>();
	 }
	 
}
