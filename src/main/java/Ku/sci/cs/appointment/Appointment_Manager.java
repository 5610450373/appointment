package Ku.sci.cs.appointment;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Appointment_Manager {
	Filemanager file;
	ArrayList<String[]> data;
	AppointmentBook book;
	FactoryApp fac;
	
	public Appointment_Manager(){
		book = new AppointmentBook();
		file = new Filemanager();
		data = new ArrayList<String[]>();	
		fac = FactoryApp.getInstace();
	}
	
	public void readdata(){
		data = file.read();
		for(String[] s : data){
			if(s[0].equals("app")){
				DateFormat dateTimeFormat = new SimpleDateFormat("dd/MM/yy HH:mm");
				try {
					Date myTime = dateTimeFormat.parse(s[1]);
					//book.add(new Onetime(myTime,s[2]));
					book.add(fac.getApp(s[3],myTime,s[2]));
					
				} catch (ParseException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
					
			}
			else{
				DateFormat dateTimeFormat2 = new SimpleDateFormat("dd/MM/yy");
				if(s[1].equals("n")||(s[1].equals("N"))){
					book.add(new Task(s[2]));	
				}
				else{
					try {
						Date myTime = dateTimeFormat2.parse(s[1]);
						book.add(new Task(myTime,s[2]));
					} catch (ParseException e1) {
						e1.printStackTrace();
					}
				}
		}
		}
	}
	
	public String getbookstring(){
		return book.toString();
	}
	
	public String add_appoint(String type,String date,String des){
		DateFormat dateTimeFormat = new SimpleDateFormat("dd/MM/yy HH:mm");	
		try {
			Date myTime = dateTimeFormat.parse(date);
			book.add(fac.getApp(type,myTime,des));
			file.write("app,"+date+","+des+","+type);
			return "MESSAGE : Add appointment is success.";
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			return "MESSAGE : Wrong date form. Please try again. (Appointment)";
		}
		
	}
	
	
	
	public String add_task(String type,String date,String des){
		file.write("task,"+date+","+des+"a");
		DateFormat dateTimeFormat2 = new SimpleDateFormat("dd/MM/yy");
		if(date.equals("n")||(date.equals("N"))){
			book.add(new Task(des));
			return "MESSAGE : Add task is success.";
		}
		else{
			try {
				Date myTime = dateTimeFormat2.parse(date);
				book.add(new Task(myTime,des));
				return "MESSAGE : Add task is success.";
			} catch (ParseException e1) {
				return "MESSAGE : Wrong date form. Please try again. (Task)";
			}
		}
	}
	
	public void clearbook(){
		book.clear();
	}
	
}
