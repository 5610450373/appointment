package Ku.sci.cs.appointment;
import java.awt.print.Book;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

//Amornthep Rojanasarit 5610450373

public class AppointmentConsole {
	private String a;
	private String b;
	private String c;
	private String d;
	
	private Appointment_Manager app_manager;
	
	public AppointmentConsole(){
		app_manager = new Appointment_Manager();
		c ="y";
	}
	public void input() throws ParseException{
		DateFormat dateTimeFormat = new SimpleDateFormat("dd/MM/yy HH:mm");
		DateFormat dateTimeFormat2 = new SimpleDateFormat("dd/MM/yy");
		System.out.println("__ADD__");
		System.out.println("<<< Appointment press 1 >>>");
		System.out.println("<<< Task press 2 >>>");
		System.out.println("<<< End press 3 >>>");
		System.out.print("Please select :");
		Scanner sc  = new Scanner(System.in);
		String select =sc.nextLine();
		if(select.equals("1")){
		System.out.println("<<< Add appointment >>>");
		System.out.print("Date (DD/MM/yy HH:mm) :");
		a = sc.nextLine();
		Date myTime = dateTimeFormat.parse(a);
		System.out.print("Description :");
		b = sc.nextLine();
		System.out.println("1.Onetime");
		System.out.println("2.Daily");
		System.out.println("3.Weekly");
		System.out.println("4.Monthly");
		System.out.print("SELECT type 1 2 3 or 4 :");
		d = sc.nextLine();
		if(d.equals("1")){
			d = "Onetime";
		}
		else if(d.equals("2")){
			d = "Daily";
		}
		else if(d.equals("3")){
			d = "Weekly";
		}
		else{
			d = "Monthly";
		}
		//book.add(fac.getApp(Integer.parseInt(d), myTime,b));
		app_manager.add_appoint(d,a,b);
		
		}
		else if (select.equals("2")){
			System.out.println("<<< Add Task >>>");
			System.out.print("Date (DD/MM/yy) or not press n :");
			a = sc.nextLine();
			System.out.print("Description :");
			b = sc.nextLine();
			app_manager.add_task("A",a,b);
		}
		System.out.print("add more? Y/N");
		c = sc.nextLine();
	}
	
	public String output1() throws ParseException{
		return app_manager.getbookstring();
	}
	
	//public String output2() throws ParseException{
		//return book.toString();
	//}
	
	public static void main(String[] args) throws ParseException{
		new GUI();
    	AppointmentConsole aptc = new AppointmentConsole();
		while(aptc.c.equals("Y") || aptc.c.equals("y")){
		try {
			aptc.input();
		} 
		catch (ParseException e) {
			System.err.println("Error date");
			aptc.c = "n";
			}
		}
		System.out.println(aptc.output1());   
	}
}
