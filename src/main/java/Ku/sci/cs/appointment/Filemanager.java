package Ku.sci.cs.appointment;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class Filemanager {
	private Connection conn;
	private Statement statement;
	public Filemanager(){
		try{
			Class.forName("org.sqlite.JDBC");
	 		String dbURL = "jdbc:sqlite:book.db";
	 		conn = DriverManager.getConnection(dbURL);
	 		statement = conn.createStatement();
		}catch (ClassNotFoundException ex) {
	 		ex.printStackTrace();
	 	}catch (SQLException ex) {
	 		ex.printStackTrace();
	 	}
	}
	public ArrayList<String[]> read(){
		ArrayList<String[]> lis = new ArrayList<String[]>();
		try{ 
	 		if (conn != null) {
	 			String query = "Select * from book";
	 			ResultSet resultSet = statement.executeQuery(query);
	 			while (resultSet.next()) {
	 				String[] str = new String[4];
	 				str[0] = resultSet.getString(1);
	 				str[1] = resultSet.getString(2);
	 				str[2] = resultSet.getString(3);
	 				if(str[0].equals("app")){
	 					str[3] = resultSet.getString(4);	 					
	 				}
	 				else{
	 					str[3] = "-";
	 				}
	 				lis.add(str);
	 			}
	 			
	 		}
	 	}catch (SQLException ex) {
	 		ex.printStackTrace();
	 	}
		return lis;
	}
	public void write(String mem){
		try{
			String[] data1 = new String[4];
			String[] data2 = mem.split(",");
			data1[0] = data2[0];
			data1[1] = data2[1];
			data1[2] = data2[2];
			if(data2[0].equals("task")){
				data1[3] = "-";
			}
			else{
				data1[3] = data2[3];
			}
		    String sql = "INSERT INTO book " +
		                 "VALUES ('"+data1[0]+"','"+data1[1]+"','"+data1[2]+"','"+data1[3]+"')";
		    statement.executeUpdate(sql);
			//statement.executeLargeUpdate("INSERT INTO book " + "VALUES ('"+data1[0]+"',data1[1],data1[2],data1[3])");
		}
		catch (Exception ex) {
	 		ex.printStackTrace();
	 	}
	}
}

	/*	FileWriter fileWriter = null;
	
	public void write(String mem){
		try {	
			fileWriter = new FileWriter("todo.txt",true);
			BufferedWriter out = new BufferedWriter(fileWriter);
			out.write(mem);
			out.newLine();
			out.flush();
		}
		catch (IOException e){
			System.err.println("Error reading from user");
		 }
	}
	
	public ArrayList<String[]> read(){
		ArrayList<String[]> lis = new ArrayList<String[]>();
		try{
			FileReader fileReader = new FileReader("todo.txt");
			BufferedReader buffer = new BufferedReader(fileReader);
			String line = buffer.readLine();
			while (line != null) {
				String[] s = line.split(",");
				lis.add(s);
				line = buffer.readLine();
			 }
	 	 }
	 	 catch (IOException e){
			 System.err.println("Error reading from file");
	 	 }
		return lis;
	}
	
	public void reset() {
		try{
		fileWriter = new FileWriter("todo.txt");
		}
		 catch (IOException e){
			 System.err.println("Error reading from user");
	 	 }
	}
}	*/
