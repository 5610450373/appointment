package Ku.sci.cs.appointment;
import java.io.IOException;
import java.util.Date;

//Amornthep Rojanasarit 5610450373

public abstract class Appointment implements appoint_task {
	String description;
	Date date;
	
	public Appointment(Date myTime,String b){
		description = b;
		date = myTime;
	}
	
	public String toString(){
		return "Appointment[date="+date+",Description="+description+"]";
	}
	
}
