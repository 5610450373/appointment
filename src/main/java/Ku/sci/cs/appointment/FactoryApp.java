package Ku.sci.cs.appointment;
//Amornthep Rojanasarit 5610450373
import java.util.Date;

public class FactoryApp {
	private static final FactoryApp instance = new FactoryApp();	
	private FactoryApp(){}	
	public static FactoryApp getInstace(){
		return instance;
	}
	
	public Appointment getApp(String type,Date d,String des){
		Appointment app = null;
		if(type.equals("Onetime")){
			app = new Onetime(d,des);
		}
		else if(type.equals("Daily")){
			app = new Daily(d,des);
		}
		else if(type.equals("Weekly")){
			app = new Weekly(d,des);
		}
		
		else if(type.equals("Monthly")){
			app = new Monthly(d,des);
		}
		
		return app;
		
	}
	
	public Appointment getApp(int type,Date d,String des){
		Appointment app = null;
		if(type==1){
			app = new Onetime(d,des);
		}
		else if(type==2){
			app = new Daily(d,des);
		}
		else if(type==3){
			app = new Weekly(d,des);
		}
		
		else if(type==4){
			app = new Monthly(d,des);
		}
		
		return app;
		
	}
}
