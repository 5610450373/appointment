package Ku.sci.cs.appointment;

import static org.junit.Assert.*;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

import org.junit.Test;
//Amornthep Rojanasarit 5610450373
public class myappointmenttest {

	@Test
	public void test() {
		DateFormat dateTimeFormat = new SimpleDateFormat("dd/MM/yy HH:mm");
		Date myTime;
		Date myTime2;
		Date myTime3;
		AppointmentBook aptb = new AppointmentBook();
		try {
			myTime = dateTimeFormat.parse("07/05/95 12:00");
			myTime2 = dateTimeFormat.parse("07/05/95 12:00");
			myTime3 = dateTimeFormat.parse("07/05/95 12:00");
			Appointment apt = new Onetime(myTime,"BD");
			Appointment apt2 = new Weekly(myTime2,"BD");
			Appointment apt3 = new Monthly(myTime3,"BD");
			assertEquals("Done",apt.toString(),"Appointment[date=Sun May 07 12:00:00 ICT 1995,Description=BD]");
			aptb.add(apt);
			assertEquals(aptb.getNumAppointment(),1);
			aptb.getAppoint(0).equals(aptb);
			assertTrue(true);
			aptb.occurOn("7/5/95");
			assertTrue(true);
			aptb.occurOn("14/5/95");
			assertTrue(true);
			aptb.occurOn("7/6/14");
			assertTrue(true);
			Task t = new Task("Hello");
			assertEquals("Done",t.toString(),"Task[Description=Hello, Status = false]");
			t.set_success();
			assertEquals("Done",t.get_status(),true);
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
